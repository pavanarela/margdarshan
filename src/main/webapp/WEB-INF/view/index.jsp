<!DOCTYPE html>
<!-- <html lang='en'>
  <head>
    <meta charset='utf-8' />

    <link href='packages/core/main.css' rel='stylesheet' />
    <link href='packages/daygrid/main.css' rel='stylesheet' />

    <script src='packages/core/main.js'></script>
    <script src='packages/daygrid/main.js'></script>
    <script src='packages/google-calendar/main.js'></script>

    <script>

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid', 'googleCalendar'],
          googleCalendarApiKey: 'AIzaSyAXNXvZ_sBwMhzQ-7bqoupHnZFin-2x-1w',
          events: [{
            googleCalendarId: 'n0e9qpsi5clhan1utvnucclieo@group.calendar.google.com',
            color: 'green',
            textColor: 'red'
          },{
        	  title: 'event 1', start: '2020-06-20', end: '2020-06-22',
              color: 'green',
              textColor: 'red'
          }],
          eventRender: function(event, element) {
              $(element).tooltip({title: event.tooltip,
                        container: 'body',
                        delay: { "show": 500, "hide": 300 }
              });
            }
        });

        calendar.render();
      });

    </script>
  </head>
  <body>

    <div id='calendar'></div>

  </body>
</html> -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resource/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resource/grid-style.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row row_paddign">
			<div class="col-sm-12"><%@ include file="menu.jsp"%></div>
		</div>
		<div class="row">
			<div class="col-sm-12"><%@ include file="slider.jsp"%></div>
		</div>
		<div class="row contact-padding">
			<div class="col-sm-12 cantact-inner-div">
				<div class="row">
					<div class="col-sm-1 align-middle" style="color: green;"></div>
					<div class="col-sm-10 border border-primary div-rotate"><marquee behavior="alternate">09828222912</marquee></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12"><%@ include file="grid.jsp"%></div>
		</div>
		<div class="row mt-3 contact-padding">		
			<div class="col-sm-12 footer">
				<%@ include file="footer.jsp"%>
			</div>			
		</div>
	</div>
</body>
</html>