<div class="ortelligance-grid-container">
 <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12 ortelligance-grid-header-top">
      <div class="ortelligance-gridHeader">
        <label class="ortelligance-icon-rounded"><i class="fa fa-folder-open ortelligance-header-icon-padding"
            aria-hidden="true"></i></label>
        <label class="ortelligance-icon-label">Case Queue Cases</label></div>
    </div>
  </div>
<div class="row">
	<div class="col-sm-12 col-xs-12 col-md-12">
		<table class="table table-striped border ortelligance-table-margin">
			<tbody>
				<tr class="ortelligance-table-header">
					<th class="border-right" scope="col">Case&nbsp;#</th>
					<th class="border-right" scope="col">Procedure</th>
					<th class="border-right" scope="col">Date</th>
					<th class="border-right" scope="col">Surgeon Name</th>

				</tr>
				<tr>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</div>
