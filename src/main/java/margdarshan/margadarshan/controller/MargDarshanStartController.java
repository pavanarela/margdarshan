package margdarshan.margadarshan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class MargDarshanStartController {	
	@GetMapping("/")
	public String getIndex() {		
		return  "index";
	}
}
