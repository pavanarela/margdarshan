package margdarshan.margdarshan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MargDarshanApplication {
	public static void main(String[] args) {
		SpringApplication.run(MargDarshanApplication.class, args);
	}
}